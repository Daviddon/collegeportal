/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import{imageserviceService}from'../../services/imageservice/imageservice.service';
import { carouselserviceService } from  '../../services/carouselservice/carouselservice.service';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */
/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */
@Component({
    selector: 'bh-departments',
    templateUrl: './departments.template.html'
})

export class departmentsComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
      imageData : any ;
  limit : any ;


    constructor(private bdms: NDataModelService,private  imgService:imageserviceService,private  cService:carouselserviceService) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.imageData = this.imgService.getImages();
    }

     ngDoCheck() { 
  this.limit = this.cService.assignLimit(0); 
  }


}

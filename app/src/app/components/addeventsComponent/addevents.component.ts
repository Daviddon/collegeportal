/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { eventservice } from 'app/sd-services/eventservice';

/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-addevents',
    templateUrl: './addevents.template.html'
})

export class addeventsComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    eventdata: FormGroup;
    constructor(private bdms: NDataModelService,private eventservice: eventservice,  private route : Router) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
         this.eventdata = new FormGroup({
            eventtopic : new FormControl('', Validators.required),
            eventdetail : new FormControl('', Validators.required),
             eventdate : new FormControl('', Validators.required)
        })
    }
    onAddevent(eventdata){
        if(this.eventdata.valid){
        this.eventservice.sendEvent(this.eventdata.value).then(data=>{
            this.route.navigate(['eventdetail']);
        })}
        else{
            window.alert("Please enter all fields");
        }
    }

}

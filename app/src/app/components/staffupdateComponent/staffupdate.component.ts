/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, Inject } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService, NSnackbarService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { adminservice } from 'app/sd-services/adminservice';

/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-staffupdate',
    templateUrl: './staffupdate.template.html'
})

export class staffupdateComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    isEdit;
    staffForm : FormGroup;
    constructor(private bdms: NDataModelService,
    public dialogRef: MatDialogRef<staffupdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private adminservice : adminservice, private snackbar :NSnackbarService) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.isEdit = this.data[1].isEdit;
        this.staffForm = new FormGroup({
            name : new FormControl(''),
            password : new FormControl(''),
            department : new FormControl(''),
            staffNumber : new FormControl(''),
            email : new FormControl(''),
            phoneNumber : new FormControl('')
        });

        this.staffForm.patchValue({
            name : this.data[0].name,
            department : this.data[0].department,
            staffNumber : this.data[0].staffNumber,
            email : this.data[0].email,
            phoneNumber : this.data[0].phoneNumber
        })
    }
    
    addStaff(data){
        this.adminservice.addStaff(data).then(data =>{
            this.snackbar.openSnackBar('Staff Added');
             this.dialogRef.close();
        })
    }

    editStaff(data){
        this.adminservice.editStaff(data).then(data =>{
            this.snackbar.openSnackBar('Staff Updated');
            this.dialogRef.close();
        })
    }

}

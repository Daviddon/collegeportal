/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, Inject } from '@angular/core';
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService, NSnackbarService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { getresult } from 'app/sd-services/getresult';
// import { resultpageComponent } from '../displayresultComponent/displayresult.component';

/**

 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-displayresult',
    templateUrl: './displayresult.template.html'
})

export class displayresultComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    result;
    constructor(private bdms: NDataModelService, 
        public dialogRef: MatDialogRef<displayresultComponent>,
        @Inject(MAT_DIALOG_DATA) public data, 
        private snackbar: NSnackbarService, 
        private getresult: getresult) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        
    }

}

/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, Inject } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { adminservice } from 'app/sd-services/adminservice';
import { staffupdateComponent } from '../staffupdateComponent/staffupdate.component';
import { Router } from '@angular/router';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-staffdetails',
    templateUrl: './staffdetails.template.html'
})

export class staffdetailsComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    staffData;
    isEdit;
    constructor(private bdms: NDataModelService,
        public dialog: MatDialog,
        private adminservice: adminservice, private router:Router) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    async ngOnInit() {
        this.staffData = this.convertObjtoArr((await this.adminservice.getStaffDetails()).local.result);
    }
    convertObjtoArr(obj) {
        return Array.from(Object.keys(obj), k => obj[k]);
    }


    addStaff() {
        const dialogRef = this.dialog.open(staffupdateComponent, {
            width: '250px',
            data: [null ,{isEdit : this.isEdit = false }]
        });

        dialogRef.afterClosed().subscribe(result => {
            this.ngOnInit()
        });
    }


    editStaff(staffData) {
        // console.log(staffData);
        const dialogRef = this.dialog.open(staffupdateComponent, {
            width: '250px',
            data: [staffData, {isEdit : this.isEdit = true } ] 
        });

        dialogRef.afterClosed().subscribe(result => {
            this.ngOnInit()
        });
    }


    deleteStaff(data) {
        console.log(data._id);
        this.adminservice.deleteStaff(data).then(data => {
            this.ngOnInit();
        })
    }
    logout(){
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        this.router.navigate(['adminlogin'])
    }
}

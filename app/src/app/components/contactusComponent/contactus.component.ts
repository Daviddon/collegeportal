/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService, NSnackbarService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { query } from 'app/sd-services/query';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-contactus',
    templateUrl: './contactus.template.html'
})
export class contactusComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    queryform: FormGroup;

    constructor(private bdms: NDataModelService, private snackbar : NSnackbarService, private query: query) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.queryform = new FormGroup({
            name: new FormControl(''),
            phone: new FormControl(''),
            email: new FormControl(''),
            query: new FormControl('')
        });
    }

    submitquery(query){
        console.log(query);
        this.query.postQuery(query).then(data => {
            this.snackbar.openSnackBar('Your Request has been recorded');
            this.queryform.reset();
        })
    }

}

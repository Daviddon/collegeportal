/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { eventservice } from 'app/sd-services/eventservice';
import { Router } from '@angular/router';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-eventsdetail',
    templateUrl: './eventsdetail.template.html'
})

export class eventsdetailComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    eventdata;
    constructor(private bdms: NDataModelService,private eventservice: eventservice,  private route : Router) {
        super();
        this.mm = new ModelMethods(bdms);
    }

   async ngOnInit() {
         this.eventdata = this.convertObjtoArr((await this.eventservice.getEvents()).local.result);
         console.log(this.eventdata);
    }
    convertObjtoArr(obj) {
        return Array.from(Object.keys(obj), k => obj[k]);
    }
    logout(){
        localStorage.removeItem('stafftoken');
        this.route.navigate(['stafflogin'])
    }
    deleteEvent(data){
        console.log(data);
        this.eventservice.deleteEvents(data).then(data=>{
            this.ngOnInit();    
        })
    }
}

/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class authserviceService implements CanActivate {
    constructor(private route: Router){

    }
    canActivate(): boolean{
        if(localStorage.getItem('token') == null){
            this.route.navigate(['/adminlogin'])
        }else{
            return true;
        }
    }

}

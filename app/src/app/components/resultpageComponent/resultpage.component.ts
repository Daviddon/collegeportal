/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService, NSnackbarService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { getresult } from 'app/sd-services/getresult';
import { displayresultComponent } from '../displayresultComponent/displayresult.component';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-resultpage',
    templateUrl: './resultpage.template.html'
})

export class resultpageComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    data;
    constructor(private bdms: NDataModelService, private getresult: getresult, private dialog: MatDialog, private snackbar : NSnackbarService) {
        super();
        this.mm = new ModelMethods(bdms);
    }
    ngOnInit() {

    }

    async showresults(hallticketnumber) {
        this.data = (await this.getresult.result({ rollnum: hallticketnumber })).local.result;
        if (this.data.status == 'success') {
            const dialogRef = this.dialog.open(displayresultComponent, {
                width: '450px',
                data: this.data.data,
                disableClose: false,
            });

            dialogRef.afterClosed().subscribe(result => {
                this.ngOnInit();
            })
        }else{
            this.snackbar.openSnackBar("Invalid Hallticket Number");
        }
    }
}

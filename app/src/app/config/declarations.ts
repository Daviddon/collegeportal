import { PageNotFoundComponent } from '../not-found.component';
import { LayoutComponent } from '../layout/layout.component';
import { ImgSrcDirective } from '../directives/imgSrc.directive';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER } from '@angular/core';
import { NDataSourceService } from '../n-services/n-dataSorce.service';
import { environment } from '../../environments/environment';
import { NMapComponent } from '../n-components/nMapComponent/n-map.component';
import { NLocaleResource } from '../n-services/n-localeResources.service';
import { NAuthGuardService } from 'neutrinos-seed-services';

window['neutrinos'] = {
  environments: environment
}

//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-queryserviceComponent
import { queryserviceComponent } from '../components/queryserviceComponent/queryservice.component';
//CORE_REFERENCE_IMPORT-displayresultComponent
import { displayresultComponent } from '../components/displayresultComponent/displayresult.component';
//CORE_REFERENCE_IMPORT-resultpageComponent
import { resultpageComponent } from '../components/resultpageComponent/resultpage.component';
//CORE_REFERENCE_IMPORT-authstaffService
import { authstaffService } from '../services/authstaff/authstaff.service';
//CORE_REFERENCE_IMPORT-addeventsComponent
import { addeventsComponent } from '../components/addeventsComponent/addevents.component';
//CORE_REFERENCE_IMPORT-eventsdetailComponent
import { eventsdetailComponent } from '../components/eventsdetailComponent/eventsdetail.component';
//CORE_REFERENCE_IMPORT-staffloginComponent
import { staffloginComponent } from '../components/staffloginComponent/stafflogin.component';
//CORE_REFERENCE_IMPORT-staffupdateComponent
import { staffupdateComponent } from '../components/staffupdateComponent/staffupdate.component';
//CORE_REFERENCE_IMPORT-authserviceService
import { authserviceService } from '../services/authservice/authservice.service';
//CORE_REFERENCE_IMPORT-staffdetailsComponent
import { staffdetailsComponent } from '../components/staffdetailsComponent/staffdetails.component';
//CORE_REFERENCE_IMPORT-adminloginComponent
import { adminloginComponent } from '../components/adminloginComponent/adminlogin.component';
//CORE_REFERENCE_IMPORT-contactusComponent
import { contactusComponent } from '../components/contactusComponent/contactus.component';
//CORE_REFERENCE_IMPORT-resultsComponent
import { resultsComponent } from '../components/resultsComponent/results.component';
//CORE_REFERENCE_IMPORT-civilComponent
import { civilComponent } from '../components/civilComponent/civil.component';
//CORE_REFERENCE_IMPORT-eeeComponent
import { eeeComponent } from '../components/eeeComponent/eee.component';
//CORE_REFERENCE_IMPORT-mechComponent
import { mechComponent } from '../components/mechComponent/mech.component';
//CORE_REFERENCE_IMPORT-eceComponent
import { eceComponent } from '../components/eceComponent/ece.component';
//CORE_REFERENCE_IMPORT-homeComponent
import { homeComponent } from '../components/homeComponent/home.component';
//CORE_REFERENCE_IMPORT-cseComponent
import { cseComponent } from '../components/cseComponent/cse.component';
//CORE_REFERENCE_IMPORT-departmentsComponent
import { departmentsComponent } from '../components/departmentsComponent/departments.component';
//CORE_REFERENCE_IMPORT-ArtImgSrcDirective
import { ArtImgSrcDirective } from '../directives/artImgSrc.directive';
//CORE_REFERENCE_IMPORT-loginComponent

//CORE_REFERENCE_IMPORT-imageserviceService
import { imageserviceService } from '../services/imageservice/imageservice.service';
//CORE_REFERENCE_IMPORT-carouselserviceService
import { carouselserviceService } from '../services/carouselservice/carouselservice.service';
//CORE_REFERENCE_IMPORT-carouselComponent
import { carouselComponent } from '../components/carouselComponent/carousel.component';
//CORE_REFERENCE_IMPORT-homeComponent


/**
 * Reads datasource object and injects the datasource object into window object
 * Injects the imported environment object into the window object
 *
 */
export function startupServiceFactory(startupService: NDataSourceService) {
  return () => startupService.getDataSource();
}

/**
*bootstrap for @NgModule
*/
export const appBootstrap: any = [
  LayoutComponent,
];


/**
*Entry Components for @NgModule
*/
export const appEntryComponents: any = [
  //CORE_REFERENCE_PUSH_TO_ENTRY_ARRAY
  
];

/**
*declarations for @NgModule
*/
export const appDeclarations = [
  ImgSrcDirective,
  LayoutComponent,
  PageNotFoundComponent,
  NMapComponent,
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-queryserviceComponent
queryserviceComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-displayresultComponent
displayresultComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-resultpageComponent
resultpageComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-addeventsComponent
addeventsComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-eventsdetailComponent
eventsdetailComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-staffloginComponent
staffloginComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-staffupdateComponent
staffupdateComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-staffdetailsComponent
staffdetailsComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-adminloginComponent
adminloginComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-contactusComponent
contactusComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-resultsComponent
resultsComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-civilComponent
civilComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-eeeComponent
eeeComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-mechComponent
mechComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-eceComponent
eceComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-homeComponent
homeComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-cseComponent
cseComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-departmentsComponent
departmentsComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-ArtImgSrcDirective
ArtImgSrcDirective,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-loginComponent

//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-loaderComponent

  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY-loginComponent
  
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY-carouselComponent
  carouselComponent,
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY-homeComponent
  
];

/**
* provider for @NgModuke
*/
export const appProviders = [
  NDataSourceService,
  NLocaleResource,
  {
    // Provider for APP_INITIALIZER
    provide: APP_INITIALIZER,
    useFactory: startupServiceFactory,
    deps: [NDataSourceService],
    multi: true
  },
  NAuthGuardService,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-authstaffService
authstaffService,
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-authserviceService
authserviceService,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY-imageserviceService
  imageserviceService,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY-carouselserviceService
  carouselserviceService

];

/**
* Routes available for bApp
*/

// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_START
export const appRoutes = [{path: 'departments', component: departmentsComponent,
children: []},{path: 'home', component: homeComponent,
children: [{path: 'departments', component: departmentsComponent},{path: 'cse', component: cseComponent},{path: 'ece', component: eceComponent},{path: 'eee', component: eeeComponent},{path: 'mech', component: mechComponent},{path: 'civil', component: civilComponent},{path: 'results', component: resultpageComponent},{path: 'contactus', component: contactusComponent}]},{path: 'resultpage', component: resultpageComponent},{path: 'displayresult', component: displayresultComponent},{path: 'adminlogin', component: adminloginComponent},{path: 'staffDetails', component: staffdetailsComponent, canActivate: [authserviceService]},{path: 'staffUpdate', component: staffupdateComponent, canActivate: [authserviceService]},{path: 'stafflogin', component: staffloginComponent},{path: 'eventdetail', component: eventsdetailComponent, canActivate: [authstaffService]},{path: 'addevents', component: addeventsComponent, canActivate: [authstaffService]},{path: '', redirectTo: '/home/departments', pathMatch: 'full'},{path: '**', component: PageNotFoundComponent}]

// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_END

import { carouselmodel } from '../src/app/models/carouselmodel.model';
import { imagedatas } from '../src/app/models/imagedatas.model';
import { user } from '../src/app/models/user.model';
//IMPORT NEW DATAMODEL

export class NDataModel {
carouselmodel: carouselmodel;
imagedatas: imagedatas;
user: user;
//DECLARE NEW VARIABLE

constructor() {
this.carouselmodel = new carouselmodel();
this.imagedatas = new imagedatas();
this.user = new user();
//CREATE NEW DM INSTANCE
    }
}
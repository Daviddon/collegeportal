/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';

@Injectable()
export class imageserviceService {
    images = [
        {
            imgSrc: "Web/photo2.jpg",

            
        },
        {
            imgSrc: "Web/photo3.jfif",
          
        },
        {
            imgSrc: "Web/gals.jpg",
            
        },
        {
            imgSrc: "Web/college.png",
           
        },
        {
            imgSrc: "Web/ambulance.jpg",
          
        },
        {
            imgSrc: "Web/build.jpg",
           
        },
        {
            imgSrc: "Web/placement.jpg",
           
        }
    ];
    sidenavList = ["Home", "Dashboard", "Contact"]
    getImages() {
        return this.images;
    }
    getHomeList() {
        return this.sidenavList;
    }

}

/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SDBaseService } from '../n-services/SDBaseService';
import { environment } from '../../environments/environment';
import {
  NAlertComponent,
  NAlertService,
  NFileIOService,
  NFileUploadComponent
} from 'neutrinos-module';
import {
  NDataModelService,
  NAuthGuardService,
  NHTTPLoaderService,
  NLocalStorageService,
  NLoginService,
  NLogoutService,
  NNotificationService,
  NPubSubService,
  NSessionStorageService,
  NSnackbarService,
  NSystemService,
  NTokenService
} from 'neutrinos-seed-services';
//CORE_REFERENCE_IMPORTS

declare const window: any;
declare const cordova: any;

@Injectable()
export class loginservice {
  systemService = NSystemService.getInstance();
  appProperties;

  constructor(
    private http: HttpClient,
    private matSnackBar: MatSnackBar,
    private sdService: SDBaseService,
    private sessionStorage: NSessionStorageService,
    private tokenService: NTokenService,
    private router: Router,
    private httpLoaderService: NHTTPLoaderService,
    private dataModelService: NDataModelService,
    private loginService: NLoginService,
    private authGuardService: NAuthGuardService,
    private localStorageService: NLocalStorageService,
    private logoutService: NLogoutService,
    private notificationService: NNotificationService,
    private pubsubService: NPubSubService,
    private snackbarService: NSnackbarService,
    private alertService: NAlertService,
    private fileIOService: NFileIOService
  ) {}

  //   service flows_loginservice

  public async adminLogin(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { modelrApiUrl: undefined, result: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_9MQJGNO6cLxP7aAA(bh);
      //appendnew_next_adminLogin
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async currentUser(data = undefined, ...others) {
    try {
      let bh = { input: { data: data }, local: { result: undefined } };
      bh = this.__constructDefault(bh);

      bh = await this.sd_9e8TO3N4aJI7aJM7(bh);
      bh = await this.sd_KNZXWwj1Cg6crXye(bh);
      //appendnew_next_currentUser
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async staffLogin(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { modelrURL: undefined, result: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_CeKIw5VTeTGi9dbw(bh);
      //appendnew_next_staffLogin
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async staffToken(token = undefined, ...others) {
    try {
      let bh = { input: { token: token }, local: {} };
      bh = this.__constructDefault(bh);

      bh = await this.sd_NM9zFh0OoIwF2s67(bh);
      //appendnew_next_staffToken
      //Start formatting output variables
      let outputVariables = { input: {}, local: {} };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_flow_loginservice_Start

  async sd_9MQJGNO6cLxP7aAA(bh) {
    try {
      bh.local.modelrApiUrl = `http://localhost:24483/api/adminLogin`;

      bh = await this.sd_DlOF1EfF9IB5wJgB(bh);
      //appendnew_next_sd_9MQJGNO6cLxP7aAA
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_DlOF1EfF9IB5wJgB(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_DlOF1EfF9IB5wJgB
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_9e8TO3N4aJI7aJM7(bh) {
    try {
      localStorage.setItem('currentUser', JSON.stringify(bh.input.data.data));
      //appendnew_next_sd_9e8TO3N4aJI7aJM7
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_KNZXWwj1Cg6crXye(bh) {
    try {
      localStorage.setItem('token', JSON.stringify(bh.input.data.token));
      //appendnew_next_sd_KNZXWwj1Cg6crXye
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_CeKIw5VTeTGi9dbw(bh) {
    try {
      bh.local.modelrURL = `http://localhost:24483/api/staffLogin`;

      bh = await this.sd_I3vxJkIqMBcb7oUv(bh);
      //appendnew_next_sd_CeKIw5VTeTGi9dbw
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_I3vxJkIqMBcb7oUv(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrURL,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_I3vxJkIqMBcb7oUv
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_NM9zFh0OoIwF2s67(bh) {
    try {
      localStorage.setItem('stafftoken', JSON.stringify(bh.input.token));
      //appendnew_next_sd_NM9zFh0OoIwF2s67
      return bh;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_node

  __constructDefault(bh) {
    const system: any = {};

    try {
      system.currentUser = this.sessionStorage.getValue('userObj');
      system.environment = environment;
      system.tokenService = this.tokenService;
      system.deviceService = this.systemService;
      system.router = this.router;
      system.httpLoaderService = this.httpLoaderService;
      system.dataModelService = this.dataModelService;
      system.loginService = this.loginService;
      system.authGuardService = this.authGuardService;
      system.localStorageService = this.localStorageService;
      system.logoutService = this.logoutService;
      system.notificationService = this.notificationService;
      system.pubsubService = this.pubsubService;
      system.snackbarService = this.snackbarService;
      system.alertService = this.alertService;
      system.fileIOService = this.fileIOService;

      Object.defineProperty(bh, 'system', {
        value: system,
        writable: false
      });

      return bh;
    } catch (e) {
      throw e;
    }
  }
}

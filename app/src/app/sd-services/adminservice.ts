/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SDBaseService } from '../n-services/SDBaseService';
import { environment } from '../../environments/environment';
import {
  NAlertComponent,
  NAlertService,
  NFileIOService,
  NFileUploadComponent
} from 'neutrinos-module';
import {
  NDataModelService,
  NAuthGuardService,
  NHTTPLoaderService,
  NLocalStorageService,
  NLoginService,
  NLogoutService,
  NNotificationService,
  NPubSubService,
  NSessionStorageService,
  NSnackbarService,
  NSystemService,
  NTokenService
} from 'neutrinos-seed-services';
//CORE_REFERENCE_IMPORTS

declare const window: any;
declare const cordova: any;

@Injectable()
export class adminservice {
  systemService = NSystemService.getInstance();
  appProperties;

  constructor(
    private http: HttpClient,
    private matSnackBar: MatSnackBar,
    private sdService: SDBaseService,
    private sessionStorage: NSessionStorageService,
    private tokenService: NTokenService,
    private router: Router,
    private httpLoaderService: NHTTPLoaderService,
    private dataModelService: NDataModelService,
    private loginService: NLoginService,
    private authGuardService: NAuthGuardService,
    private localStorageService: NLocalStorageService,
    private logoutService: NLogoutService,
    private notificationService: NNotificationService,
    private pubsubService: NPubSubService,
    private snackbarService: NSnackbarService,
    private alertService: NAlertService,
    private fileIOService: NFileIOService
  ) {}

  //   service flows_adminservice

  public async getStaffDetails(...others) {
    try {
      let bh = {
        input: {},
        local: { result: undefined, modelrApiUrl: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_u91qd4RieFQNbutT(bh);
      //appendnew_next_getStaffDetails
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async deleteStaff(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { result: undefined, modelrApiUrl: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_tUBdtTNujYu15Rve(bh);
      //appendnew_next_deleteStaff
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async addStaff(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { result: undefined, modelrApiUrl: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_t0XSNZWO5ZuHifyS(bh);
      //appendnew_next_addStaff
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async editStaff(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { result: undefined, modelrApiUrl: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_OAALVNtcgRaztYGu(bh);
      //appendnew_next_editStaff
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_flow_adminservice_Start

  async sd_u91qd4RieFQNbutT(bh) {
    try {
      bh.local.modelrApiUrl = `http://localhost:24483/api/getStaffDetails`;

      bh = await this.sd_Q6mUbgq0bgeT3TEd(bh);
      //appendnew_next_sd_u91qd4RieFQNbutT
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_Q6mUbgq0bgeT3TEd(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: undefined
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_Q6mUbgq0bgeT3TEd
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_tUBdtTNujYu15Rve(bh) {
    try {
      bh.local.modelrApiUrl = `http://localhost:24483/api/deleteStaff`;

      bh = await this.sd_8YDJkiRAhPY7MdqT(bh);
      //appendnew_next_sd_tUBdtTNujYu15Rve
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_8YDJkiRAhPY7MdqT(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_8YDJkiRAhPY7MdqT
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_t0XSNZWO5ZuHifyS(bh) {
    try {
      bh.local.modelrApiUrl = `http://localhost:24483/api/addStaff`;

      bh = await this.sd_DzHibYXLp2bwj5hT(bh);
      //appendnew_next_sd_t0XSNZWO5ZuHifyS
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_DzHibYXLp2bwj5hT(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_DzHibYXLp2bwj5hT
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_OAALVNtcgRaztYGu(bh) {
    try {
      bh.local.modelrApiUrl = `http://localhost:24483/api/editStaff`;

      bh = await this.sd_2pcWi32hWLcMwAmC(bh);
      //appendnew_next_sd_OAALVNtcgRaztYGu
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_2pcWi32hWLcMwAmC(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_2pcWi32hWLcMwAmC
      return bh;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_node

  __constructDefault(bh) {
    const system: any = {};

    try {
      system.currentUser = this.sessionStorage.getValue('userObj');
      system.environment = environment;
      system.tokenService = this.tokenService;
      system.deviceService = this.systemService;
      system.router = this.router;
      system.httpLoaderService = this.httpLoaderService;
      system.dataModelService = this.dataModelService;
      system.loginService = this.loginService;
      system.authGuardService = this.authGuardService;
      system.localStorageService = this.localStorageService;
      system.logoutService = this.logoutService;
      system.notificationService = this.notificationService;
      system.pubsubService = this.pubsubService;
      system.snackbarService = this.snackbarService;
      system.alertService = this.alertService;
      system.fileIOService = this.fileIOService;

      Object.defineProperty(bh, 'system', {
        value: system,
        writable: false
      });

      return bh;
    } catch (e) {
      throw e;
    }
  }
}

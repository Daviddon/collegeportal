/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { BehaviorSubject } from 'rxjs';
@Injectable()
export class carouselserviceService {
    limitImage = new BehaviorSubject(2);
    numberOfImage = this.limitImage.asObservable();

    constructor(public mediaObserver: MediaObserver) {
        this.mediaObserver.asObservable().subscribe((change: MediaChange[]) => {
            this.limitImage.next(this.assignLimit(change[0].mqAlias));
        });
    }
    assignLimit(mqAlias): number {
        if (mqAlias === 'xs') {
            return 1;
        } else if (mqAlias === 'sm') {
            return 2;
        } else if ((mqAlias === 'lg') || (mqAlias === 'md')) {
            return 3;
        } else {
            return 4;
        }
    }

}

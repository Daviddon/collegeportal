import { SDBaseService } from 'app/n-services/SDBaseService';
//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-query
import { query } from '../sd-services/query';
//CORE_REFERENCE_IMPORT-getresult
import { getresult } from '../sd-services/getresult';
//CORE_REFERENCE_IMPORT-eventservice
import { eventservice } from '../sd-services/eventservice';
//CORE_REFERENCE_IMPORT-adminservice
import { adminservice } from '../sd-services/adminservice';
//CORE_REFERENCE_IMPORT-loginservice
import { loginservice } from '../sd-services/loginservice';

export const sdProviders = [
  SDBaseService,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-query
  query,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-getresult
  getresult,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-eventservice
  eventservice,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-adminservice
  adminservice,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-loginservice
  loginservice
];

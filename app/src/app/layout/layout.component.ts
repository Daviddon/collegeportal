import { Component, OnInit } from '@angular/core';
import { NTokenService, NLoginService, NLocalStorageService } from 'neutrinos-seed-services';
import { Router } from '@angular/router'

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>
             <n-snackbar></n-snackbar>`
})
export class LayoutComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  constructor(private nLocalstorage: NLocalStorageService,
    private nTokenService: NTokenService,
    private loginservice: NLoginService,
    private router: Router) { }
  ngOnInit() {
    if (this.nLocalstorage.getValue('accessToken')) {
      this.nTokenService.updateSessionStorage();
    }
  }
}

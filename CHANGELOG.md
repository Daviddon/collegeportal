##6.1.1 (26/08/2019)
    - Use "fs" instead of "fs-extra" in gulpfile.js

##6.1.0 (24/08/2019)
    - migrated to studio v6.1.0
    - Fix memoryleak due to multiple subscriptions in the service.

##6.0.1 (01/07/2019)
    - migrated to Studio V6.0.1

##5.0.4
    - migrated to Studio V5.0.4

##5.0.1
    - migrated to Studio V5.0.1

##3.1.0
    - migrated to 4.1.0

##3.0.3
    - migrated to 4.0.4

##3.0.1
    - manually migrated to 4.0.3 from 4.0.2
    - Added appIcon

##3.0.0
    - manually migrated to 4.0.2 from 4.0.1

##2.0.0
    - manually migrated to 4.0.1 from 4.0.0-beta_3
    - changed AutohideSplashScreen Preference to false
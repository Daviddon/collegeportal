/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
@Injectable()
export class authstaffService implements CanActivate {
    
    constructor(private route: Router){

    }
    canActivate(): boolean{
        if(localStorage.getItem('stafftoken') == null){
            this.route.navigate(['/stafflogin'])
        }else{
            return true;
        }
    }
}
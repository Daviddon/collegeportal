/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { loginservice } from 'app/sd-services/loginservice';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-stafflogin',
    templateUrl: './stafflogin.template.html'
})

export class staffloginComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    loginForm : FormGroup;
    constructor(private bdms: NDataModelService, private loginService : loginservice,
     private route : Router) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.loginForm = new FormGroup({
            email : new FormControl('', [Validators.required, Validators.email]),
            password : new FormControl('', Validators.required)
        })
    }
    async login(data){
         var userData = (await this.loginService.staffLogin(data)).local.result;
        console.log(userData);
        if(userData.status == "success"){
            console.log(userData);
            this.loginService.staffToken(userData.token).then(data => {
                this.route.navigate(['/eventdetail']);
            })
        }

    }
}
